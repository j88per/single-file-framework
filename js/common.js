/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // get header
    $.get('templates/nav.html', function (data) {
        $("#topNav").empty();
        $("#topNav").append(data);
    });

    $(".nav-menu").each(function () {
        $(this).click(function () {
            var menuItem = $(this).text();
            var item = menuItem.replace(" ", "_").toLowerCase();
            clearNav();


            $.get('/menu/' + item, function (data) {
                $("#content").hide();
                if ($("#stuff").is(":visible")) {
                    $("#stuff").empty();
                } else {
                    $("#content").after('<div id="stuff"></div>');
                }
                $("#stuff").append(data);
            });
        });
    });


});

/**
 * clear navigation state
 * @returns {undefined}
 */
function clearNav()
{
    $(".nav-pills li").each(function () {
        $(this).removeClass('active');
    });
}

/**
 * handle simple validation on front-end, more on back-end
 * @param {string} $field
 * @returns {true|false}
 */
function validate(field, validType)
{
    var isValid = false;

    switch (validType)
    {
        case 'email':
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (re.test(field)) {
                isValid = true;
            }
            break;

        case 'text':
            if (field.length >= 2) { // tests length first
                isValid = true;
            }

            break;

        case 'password':
            if (field.match((/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/))) {
                isValid = true;
            }
            break;
    }

    return isValid;
}