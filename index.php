<?php

/**
 * Single File Framework - base class
 *
 * @category   Frameworks 
 * @package    Single File Framework
 * @author     Andrew Carlson <acarlson@aamkgroup.net>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)

namespace Singlefileframework;

/**
 * the main class.  There shouldn't be any other core framework classes.
 */
class Singlefileframework
{

    /**
     * enable caching true/false
     * @var type bool
     */
    protected $caching = false;

    /**
     * place to store request params
     */
    protected $request = '';

    /**
     * the page the end user sees
     * @var string $page
     */
    protected $page = '';

    /**
     * handle system messages
     * @var object
     */
    protected $msg = '';

    /**
     * result state, success/fail used for css on front-end
     * @var object
     */
    protected $state = '';

    /**
     * use/load composer auto loader
     * @var bool $composer
     */
    protected $composer = true;

    /**
     * set db access parameters
     * @var string
     */
    protected $dbname = 'sff_blog';
    protected $dbuser = 'blog_usr';
    protected $dbpass = 'passw0rd';
    protected $dbhost = 'localhost';
    protected $dbdriver = 'pdo_mysql';

    /**
     * create a db connection object
     * @var object
     */
    protected $dbConnection = '';

    /**
     * start output buffers and build base page
     */
    public function __construct()
    {
        if ($this->composer) {
            // put composer autoloader here
            require_once 'vendor/autoload.php';
        }

        if ($this->caching) {
            ob_start('ob_gzhandler');
        }

        if ((isset($this->getRequest()->isAjax)) && ($this->getRequest()->isAjax == true)) {
            // handle ajax responses
            $action = $this->getMethod();
            if (!method_exists($this, $action)) {
                echo json_encode(array("msg" => "no data"));
            } else {
                $this->$action();
            }
        } else {
            $action = $this->getMethod();
            if (!method_exists($this, $action)) {
                $this->createPage();
            } else {
                $this->$action();
            }
        }
    }

    public function __destruct()
    {
        if ((isset($this->getRequest()->isAjax)) && ($this->getRequest()->isAjax == true)) {
            if ($this->msg != '') {
                echo json_encode(array('msg' => $this->msg, 'state' => $this->state));
            }
            exit(0);
        } else {
            // non-ajax responses
            $this->makePage();
            echo $this->page;
        }

        if ($this->caching) {
            ob_end_flush();
        }
    }

    /**
     * get GET/POST request params
     */
    private function getRequest()
    {
        $this->getRequest = new \Stdclass();

        foreach ($_REQUEST as $key => $param) {
            $this->getRequest->$key = $param;
        }

        // @todo handle normal urls
        $uri = $_SERVER['REQUEST_URI'];
        $path = explode("/", $uri);
        array_shift($path);
        $object = array_shift($path);

        foreach ($path as $item) {
            $value = $item;
        }

        if (isset($value)) {
            $this->getRequest->$object = $value;
        }

        return $this->getRequest;
    }

    /**
     * load in the function to perform dynamically
     * or throw a 404
     */
    public function getMethod()
    {
        $url = $_SERVER['REQUEST_URI'];

        $path = explode("/", $url);
        $method = $path[1];

        if (method_exists($this, $method)) {
            return $method;
        } else {
            return false;
        }
    }

    /**
     * function to create a default page object
     * 
     * We use an object to define the content areas on the page
     */
    protected function createPage()
    {
        $page = new \StdClass();
        $page->header = file_get_contents('templates/header.html');
        $page->nav = file_get_contents('templates/nav.html');
        $page->messages = '<div id="messages" class="col-md-9"></div>';
        $page->body = file_get_contents('templates/index.html');
        $page->footer = file_get_contents('templates/footer.html');

        $this->pageOutput = $page;
    }

    public function makePage()
    {
        $pageDoc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $html = '';

        foreach ($this->pageOutput as $block) {
            $html .= $block;
        }

        $pageDoc->loadHTML($html);
        $this->page = $pageDoc->saveHTML();
    }

    public function page()
    {
        $page = new \StdClass();
        $template = $this->getRequest()->page;
        $templateFile = file_get_contents('templates/' . $template . '.html');
        $page->body = $this->fillPage($template, $templateFile); // fill page (contextual?)
        $this->pageOutput = $page;
    }

    /**
     * if a template has content, grab it from db and fill template
     * 
     * @param type $template
     * @param type $sourceData
     */
    protected function fillPage($sourceData, $template)
    {
        $pageBody = '';

        // set db connection
        if ((!isset($this->dbConnection)) || ($this->dbConnection == '')) {
            $this->dbConnect();
        }

        // fill page with content

        return $pageBody;
    }

    /**
     * display data on screen in debug format
     * 
     * @param mixed $data
     */
    protected function debug($data = '')
    {
        echo "<pre class='col-md-9'>";
        print_r($data);
        echo "</pre>";
        $this->pageOutput = '<span></span>';
        die;
    }

    /**
     * connect to db via dbal
     */
    protected function dbConnect()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'dbname' => $this->dbname,
            'user' => $this->dbuser,
            'password' => $this->dbpass,
            'host' => $this->dbhost,
            'driver' => $this->dbdriver
        );
        $this->dbConnection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        return;
    }

}

new Singlefileframework();
?>

